#!/usr/bin/env ruby

def kaprekar
  array_of_kaprekar = []
  1.upto(1000) do |k|
    n = k.to_s.length
    
    k_squared = k ** 2
    k_squared_array = k_squared.to_s.chars.map(&:to_i) 
    
    int1_arr = k_squared_array[0, k_squared_array.length - n]
    int2_arr = k_squared_array[k_squared_array.length - n..-1]
    
    int1_str = int1_arr.join
    int2_str = int2_arr.join
    
    int1 = int1_str.to_i
    int2 = int2_str.to_i
    
    sum = int1 + int2
    
    if sum == k
      array_of_kaprekar << k
    end
  end
  puts array_of_kaprekar
end

kaprekar